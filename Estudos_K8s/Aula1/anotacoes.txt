## Bashrc to autocomplete
´´´
cat >>  ~/.bashrc << EOF
source <(kubectl completion bash)
source <(kubeadm completion bash)
alias k=kubectl
complete -F __start_kubectl k
EOF
´´´

