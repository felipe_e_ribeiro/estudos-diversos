# Create secret for pod-secret.yaml
kubectl create secret generic my-literal-secret --from-literal user=felipe --from-literal password=mudar123@

# Create confmap for ds_nginx.yml 
 kubectl create configmap nginx-config --from-file=index.html

# Create service account felipe  
 kubectl create serviceaccount felipe

# Create Cluster role for felipe and give cluster-admin
 kubectl create clusterrolebinding felips --serviceaccount=default:felipe --clusterrole=cluster-admin

