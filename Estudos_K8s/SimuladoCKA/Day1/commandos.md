## Create namespace called ribeiro
	kubectl create namespace ribeiro

##  Run simple pod nginx called felps using image 1.18.0 on 80/TCP at namespace ribeiro
	kubectl run --image=nginx:1.18.0 felps --namespace=ribeiro --port=80 --dry-run=client -o yaml
```
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: felps
  name: felps
  namespace: ribeiro
spec:
  containers:
  - image: nginx:1.18.0
    name: web
    resources: {}
    ports:
    - containerPort: 80
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

## Run a deployment called felps-dp using image nginx:1.18 using port 80
	kubectl create deployment felps-dp --image=nginx:1.18.0 --port=80
```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: felps-dp
  name: felps-dp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: felps-dp
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: felps-dp
    spec:
      containers:
      - image: nginx:1.18.0
        name: nginx
        ports:
        - containerPort: 80
        resources: {}
status: {}
```

## Change the replica number of deployment felps-dp on namespace default from 1 to 3
kubectl scale deployment felps-dp --replicas=3
```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: felps-dp
  name: felps-dp
spec:
  replicas: 3
  selector:
    matchLabels:
      app: felps-dp
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: felps-dp
    spec:
      containers:
      - image: nginx:1.18.0
        name: nginx
        ports:
        - containerPort: 80
        resources: {}
status: {}
```


## Upgrade version of pod felps from 1.18.0 to 1.21.1

```
kubectl get pod felps -o yaml > 03_example_pod.yml
### change the version of 1.18.0 to 1.21.1 using vi/vim/nano. After that, use:
kubectl apply -f 03_example_pod.yml

or

kubectl edit pod felps
### (Change) the value of nginx:

or

kubectl set image pod/felps felps=nginx:1.21.0
```
