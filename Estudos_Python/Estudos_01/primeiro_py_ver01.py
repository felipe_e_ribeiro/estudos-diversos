#!/usr/bin/env python3
import sys

args = sys.argv[1:]
options = {}
for arg in args:
    key, value = arg.split("=")
    options[key.lstrip("--")] = value

langs = {
        "en": "Hello World",
        "it": "Ciao Mondo",
        "pt": "Ola mundão"
}

msg = langs[options["lang"]]
nome = options["nome"]

print(f"{msg}, {nome}")