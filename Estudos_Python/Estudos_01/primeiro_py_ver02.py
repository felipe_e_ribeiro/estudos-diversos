#!/usr/bin/env python3
import os
import sys
import argparse

parser = argparse.ArgumentParser(description="Hello World Script")
parser.add_argument("--nome", default=os.getenv("NOME"))
parser.add_argument("--lang", default=os.getenv("LANG"))
options = parser.parse_args()


langs = {
        "en": "Hello World",
        "it": "Ciao Mondo",
        "pt": "Ola mundão"
}

msg = langs[options.lang]
nome = options.nome

print(f"{msg}, {nome}")